﻿
// OwnDrawDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "OwnDraw.h"
#include "OwnDrawDlg.h"
#include "afxdialogex.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
int n = 0;

// COwnDrawDlg 对话框



COwnDrawDlg::COwnDrawDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_OWNDRAW_DIALOG, pParent), m_test_btn(L"./res/normal_close.bmp", L"./res/horver_close.bmp", L"./res/push_close.bmp"),
	m_maxbtn(L"./res/normal_max.bmp", L"./res/push_max.bmp", L"./res/push_max.bmp"),
	m_minbtn(L"./res/normal_min.bmp", L"./res/push_min.bmp", L"./res/push_min.bmp")
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void COwnDrawDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_TEST, m_test_btn);
	DDX_Control(pDX, IDC_BTN_MIN, m_minbtn);
	DDX_Control(pDX, IDC_BTN_MAX, m_maxbtn);
}

BEGIN_MESSAGE_MAP(COwnDrawDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &COwnDrawDlg::OnBnClickedOk)
	ON_WM_NCHITTEST()
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BTN_TEST, &COwnDrawDlg::OnBnClickedBtnTest)
	ON_BN_CLICKED(IDC_BTN_MIN, &COwnDrawDlg::OnBnClickedBtnMin)
	ON_BN_CLICKED(IDC_BTN_MAX, &COwnDrawDlg::OnBnClickedBtnMax)
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()


// COwnDrawDlg 消息处理程序

BOOL COwnDrawDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	CRect rect;
	GetClientRect(&rect);
	m_clientRect = m_captionRect = rect;
	m_captionRect.bottom = 30;
	m_clientRect.top = 30;

	m_minbtn.MoveWindow(rect.right - 102, 0, 32, 30);
	m_maxbtn.MoveWindow(rect.right - 70, 0, 32, 30);
	m_test_btn.MoveWindow(rect.right - 38, 0, 38, 30);
	SetWindowTitle(L"腾讯QQ");

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void COwnDrawDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR COwnDrawDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void COwnDrawDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	//CDialogEx::OnOK();
}

void COwnDrawDlg::DrawCaption(CDC* pDc) {
	
	CRect rect;
	GetClientRect(&rect);
	rect.bottom = 30;
	m_captionRect = rect;
	pDc->FillSolidRect(&m_captionRect, RGB(240,253,255));
	DrawIconEx(*pDc, 4, 4, m_hIcon, 20, 20, 0,NULL, DI_MASK | DI_IMAGE);
	CFont font;
	font.CreatePointFont(100, L"楷体");
	pDc->SelectObject(&font);
	pDc->SetBkMode(TRANSPARENT);
	pDc->TextOut(26, 4, m_strTitleText);
}
void COwnDrawDlg::SetWindowTitle(LPCTSTR lptitle) {
	m_strTitleText = lptitle;
}

LRESULT COwnDrawDlg::OnNcHitTest(CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	ScreenToClient(&point);   //细节
	LRESULT result = CDialogEx::OnNcHitTest(point);
	if (result == HTCLIENT&&m_captionRect.PtInRect(point)) {
		result = HTCAPTION;
	}
	return result;
}


BOOL COwnDrawDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CRect rect;
	GetClientRect(&rect);
	rect.top = 30;
	m_captionRect = rect;
	DrawCaption(pDC);
	pDC->FillSolidRect(&rect, RGB(255, 255, 255));

	

	return TRUE;
	//return CDialogEx::OnEraseBkgnd(pDC);
}


void COwnDrawDlg::OnBnClickedBtnTest()
{
	// TODO: 在此添加控件通知处理程序代码
	EndDialog(IDCANCEL);
}


void COwnDrawDlg::OnBnClickedBtnMin()
{
	// TODO: 在此添加控件通知处理程序代码
	CloseWindow();
}


void COwnDrawDlg::OnBnClickedBtnMax()
{
	// TODO: 在此添加控件通知处理程序代码
	if (IsZoomed()) {
		ShowWindow(SW_NORMAL);
	}
	else {
		ShowWindow(SW_MAXIMIZE);
	}
	//Invalidate(TRUE);
}


void COwnDrawDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);
	CRect rect;
	GetClientRect(&rect);
	if (m_maxbtn.GetSafeHwnd()) {
		m_minbtn.MoveWindow(rect.right - 102, 0, 32, 30);
		m_maxbtn.MoveWindow(rect.right - 70, 0, 32, 30);
		m_test_btn.MoveWindow(rect.right - 38, 0, 38, 30);
	}
	// TODO: 在此处添加消息处理程序代码
}

void COwnDrawDlg::drawline(CPoint p0, CPoint p1) {
	CDC* pDC = GetDC();
	CRect rect;
	GetClientRect(&rect);	//获取客户端坐标
	pDC->SetMapMode(MM_ANISOTROPIC);//设置映射模式
	pDC->SetWindowExt(rect.Width(), rect.Height()); //设置窗口
	pDC->SetViewportExt(rect.Width(), -rect.Height());	//设置视区，
	pDC->SetViewportOrg(rect.Width() / 2, rect.Height() / 2);
	rect.OffsetRect(-rect.Width() / 2, -rect.Height() / 2);
	//-----------------------------------------------------------

	CPoint p, temp;
	int dx = p1.x - p0.x;
	int dy = p1.y - p0.y;
	double k = (dy * 1.00) / (dx * 1.00);//计算斜率

	if (dx == 0)//垂线
	{
		if (dy < 0)//起点在上方，调换
		{
			temp = p0;
			p0 = p1;
			p1 = temp;
		}
		for (p = p0; p.y < p1.y; p.y++)//主移动方向->y,不包括p1
		{
			pDC->SetPixelV(p.x, p.y, RGB(0, 0, 0));
		}
	}

	else
	{
		double e = 0.00;//增量

		if (k >= 0 && k <= 1)
		{
			if (dx < 0)//p1在左侧，调换
			{
				temp = p0;
				p0 = p1;
				p1 = temp;
			}//p0在左下

			for (p = p0; p.x < p1.x; p.x++)//主移动方向->x,不包括p1
			{
				pDC->SetPixelV(p.x, p.y, RGB(e * 255, e * 255, e * 255));
				pDC->SetPixelV(p.x, p.y + 1, RGB((1 - e) * 255, (1 - e) * 255, (1 - e) * 255));//不同亮度值
				e += k;

				if (e >= 1.0)
				{
					p.y++;
					e -= 1;
				}
			}
			/*p0.x+=10;
			p1.x+=10;
			pDC->MoveTo(p0);
			pDC->LineTo(p1);*/
		}
		else if (k > 1)
		{
			if (dy < 0)//p1在左侧，调换
			{
				temp = p0;
				p0 = p1;
				p1 = temp;
			}//p0在下方

			for (p = p0; p.y < p1.y; p.y++)//主移动方向->y,不包括p1
			{
				pDC->SetPixelV(p.x, p.y, RGB(e * 255, e * 255, e * 255));
				pDC->SetPixelV(p.x + 1, p.y, RGB((1 - e) * 255, (1 - e) * 255, (1 - e) * 255));
				e += 1.00 / (k * 1.00);

				if (e >= 1.0)
				{
					p.x++;
					e -= 1;
				}
			}
		}

		else if (k >= -1 && k < 0)
		{
			e = 0.00;
			if (dx < 0)//p1在左上，调换
			{
				temp = p0;
				p0 = p1;
				p1 = temp;
			}//p0在左上

			for (p = p0; p.x < p1.x; p.x++)//主移动方向->x,不包括p1
			{
				pDC->SetPixelV(p.x, p.y, RGB(-1 * e * 255, -1 * e * 255, -1 * e * 255));
				pDC->SetPixelV(p.x, p.y - 1, RGB((1 + e) * 255, (1 + e) * 255, (1 + e) * 255));//这里e是负数！！！
				e += k;

				if (e <= -1.0)
				{
					p.y--;
					e += 1.0;
				}
			}
		}

		else if (k < -1)
		{
			if (dy > 0)//p1在上方，调换
			{
				temp = p0;
				p0 = p1;
				p1 = temp;
			}//p0在上
			for (p = p0; p.y > p1.y; p.y--)//主移动方向->y,不包括p1
			{
				pDC->SetPixelV(p.x, p.y, RGB(e * 255, e * 255, e * 255));
				pDC->SetPixelV(p.x + 1, p.y, RGB((1 - e) * 255, (1 - e) * 255, (1 - e) * 255));
				e += -1.0 / (k * 1.0);

				if (e >= 1.0)
				{
					p.x++;
					e -= 1;
				}
			}
		}

	}
	//pDC->DeleteDC();
}

void COwnDrawDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	n++;
	CPoint p1, p2;
	if (n % 2 == 0) {
		p2.x = -point.x / 2;
		p2.y = point.y / 2;
		p1.x = -p2.x + 10;
		p1.y = -p2.y + 10;
	}
	else {
		p2.x = point.x / 2;
		p2.y = point.y / 2;
		p1.x = -p2.x - 10;
		p1.y = -p2.y - 10;
	}
	

	drawline(p1, p2);
	CDialogEx::OnLButtonDown(nFlags, point);
}
