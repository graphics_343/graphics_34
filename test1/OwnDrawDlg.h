﻿
// OwnDrawDlg.h: 头文件
//

#pragma once
#include "CButtonUI.h"

// COwnDrawDlg 对话框
class COwnDrawDlg : public CDialogEx
{
// 构造
public:
	COwnDrawDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_OWNDRAW_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString m_strTitleText;
	CRect m_captionRect;
	CRect m_clientRect;
	afx_msg void OnBnClickedOk();
	void DrawCaption(CDC* pDc);
	void SetWindowTitle(LPCTSTR lptitle);
	CButtonUI m_test_btn;
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	CButtonUI m_minbtn;
	CButtonUI m_maxbtn;
	afx_msg void OnBnClickedBtnTest();
	afx_msg void OnBnClickedBtnMin();
	afx_msg void OnBnClickedBtnMax();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	void drawline(CPoint p0, CPoint p1);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};
