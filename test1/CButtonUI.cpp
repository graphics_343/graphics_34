#include "pch.h"
#include "CButtonUI.h"
BEGIN_MESSAGE_MAP(CButtonUI, CButton)
	ON_WM_MOUSELEAVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

CButtonUI::CButtonUI(LPCTSTR lpnormalbitmap, LPCTSTR lphoverbitmap, LPCTSTR lpdownbitmap) {
	m_nomalbitmap = (HBITMAP)LoadImage(AfxGetInstanceHandle(), lpnormalbitmap, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	m_hoverbitmap = (HBITMAP)LoadImage(AfxGetInstanceHandle(), lphoverbitmap, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	m_pushbitmap=(HBITMAP)LoadImage(AfxGetInstanceHandle(), lpdownbitmap, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	m_IsinButton=FALSE;
	m_IsDownButton=FALSE;

}
CButtonUI::~CButtonUI() {
}


void CButtonUI::PreSubclassWindow()
{

	ModifyStyle(0, BS_OWNERDRAW);
	CButton::PreSubclassWindow();
}


void CButtonUI::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{

	CDC dc;
	dc.Attach(lpDrawItemStruct->hDC);
	CRect rect = lpDrawItemStruct->rcItem;
	
	if (m_IsDownButton && m_IsinButton) {
		CDC memdc;
		memdc.CreateCompatibleDC(&dc);
		memdc.SelectObject(m_pushbitmap);
		dc.BitBlt(0, 0, rect.Width(), rect.Height(), &memdc, 0, 0, SRCCOPY);
	}
	else if (!m_IsDownButton && m_IsinButton) {
		CDC memdc;
		memdc.CreateCompatibleDC(&dc);
		memdc.SelectObject(m_hoverbitmap);
		dc.BitBlt(0, 0, rect.Width(), rect.Height(), &memdc, 0, 0, SRCCOPY);
	}
	else {
		CDC memdc;
		memdc.CreateCompatibleDC(&dc);
		memdc.SelectObject(m_nomalbitmap);
		dc.BitBlt(0, 0, rect.Width(), rect.Height(), &memdc, 0, 0, SRCCOPY);
	}



	dc.Detach();
}


void CButtonUI::OnMouseLeave()
{
	
	m_IsDownButton = FALSE;
	m_IsinButton = FALSE;
	Invalidate(TRUE);
	CButton::OnMouseLeave();
}


void CButtonUI::OnLButtonDown(UINT nFlags, CPoint point)
{
	
	m_IsDownButton = TRUE;
	Invalidate(TRUE);
	CButton::OnLButtonDown(nFlags, point);
}


void CButtonUI::OnMouseMove(UINT nFlags, CPoint point)
{
	
	if (m_IsinButton == FALSE) {
		TRACKMOUSEEVENT mouseEvent;
		mouseEvent.cbSize = sizeof(TRACKMOUSEEVENT);
		mouseEvent.dwHoverTime = 10;
		mouseEvent.dwFlags = TME_LEAVE;
		mouseEvent.hwndTrack = m_hWnd;
		_TrackMouseEvent(&mouseEvent);
		m_IsinButton = TRUE;
	}
	Invalidate(TRUE);
	CButton::OnMouseMove(nFlags, point);
}


void CButtonUI::OnLButtonUp(UINT nFlags, CPoint point)
{

	m_IsDownButton = FALSE;
	Invalidate(TRUE);
	CButton::OnLButtonUp(nFlags, point);
}
