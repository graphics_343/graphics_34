// Light.h: interface for the CLight class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LIGHT_H__ACA1A2A6_E72D_4EBC_B919_5C0AC538F456__INCLUDED_)
#define AFX_LIGHT_H__ACA1A2A6_E72D_4EBC_B919_5C0AC538F456__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "P3.h"

class CLight  
{
public:
	CLight();
	virtual ~CLight();
	void SetDiffuse(CRGB);
	void SetSpecular(CRGB);
	void SetPosition(double,double,double);
	void SetGlobal(double,double,double);
	void SetCoef(double,double,double);
	void SetOnOff(BOOL);
	void GlobalToXYZ();
public:
	CRGB L_Diffuse;
	CRGB L_Specular;
	CP3  L_Position;
	double L_R,L_Phi,L_Theta;
	double L_C0;
	double L_C1;
	double L_C2;
	BOOL L_OnOff;
};

#endif // !defined(AFX_LIGHT_H__ACA1A2A6_E72D_4EBC_B919_5C0AC538F456__INCLUDED_)
