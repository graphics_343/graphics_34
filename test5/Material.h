// Material.h: interface for the CMaterial class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MATERIAL_H__1161180E_9A92_4DAE_AB2E_757671AB6E12__INCLUDED_)
#define AFX_MATERIAL_H__1161180E_9A92_4DAE_AB2E_757671AB6E12__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "RGB.h"

class CMaterial  
{
public:
	CMaterial();
	virtual ~CMaterial();
	void SetAmbient(CRGB);
	void SetDiffuse(CRGB);
	void SetSpecular(CRGB);
	void SetEmit(CRGB);
	void SetExp(double);
public:
	CRGB M_Ambient;
	CRGB M_Diffuse;
	CRGB M_Specular;
	CRGB M_Emit;
	double M_n;
};

#endif // !defined(AFX_MATERIAL_H__1161180E_9A92_4DAE_AB2E_757671AB6E12__INCLUDED_)
