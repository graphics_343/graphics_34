// ZBuffer.h: interface for the CZBuffer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ZBUFFER_H__ED93E842_7399_49B8_B579_6A52FBA1289D__INCLUDED_)
#define AFX_ZBUFFER_H__ED93E842_7399_49B8_B579_6A52FBA1289D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Pi3.h"
#include "Bucket.h"
#include "Vector.h"
#include "Lighting.h"
#include "Material.h"

class CZBuffer  
{
public:
	CZBuffer();
    virtual ~CZBuffer();
	void CreateBucket();
	void CreateEdge();
	void AddEt(CAET *);
	void ETOrder();
	void Phong(CDC *,CP3,CLighting *,CMaterial *,COLORREF **);
	void InitDeepBuffer(int,int,double);
	CRGB Interpolation(double,double,double,CRGB,CRGB);
	CVector Interpolation(double,double,double,CVector,CVector);
	CT2 Interpolation(double,double,double,CT2,CT2);
	void SetPoint(CPi3 *,CVector *,CT2 *,int);
	void ClearMemory();
	void DeleteAETChain(CAET* pAET);
protected:
	int PNum;
	CPi3 *P;
	CT2 *T;
	CVector *N;
	CAET *pHeadE,*pCurrentE,*pEdge;
	CBucket *pCurrentB,*pHeadB;
	double **zBuffer;
	int Width,Height;
};

#endif // !defined(AFX_ZBUFFER_H__ED93E842_7399_49B8_B579_6A52FBA1289D__INCLUDED_)
