// TestView.h : interface of the CTestView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTVIEW_H__A75FDCFB_621C_4E38_A154_C344803E6372__INCLUDED_)
#define AFX_TESTVIEW_H__A75FDCFB_621C_4E38_A154_C344803E6372__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Line.h"//包含直线类
#include "Transform.h"
#include "P2.h"	// Added by ClassView

class CTestView : public CView
{
protected: // create from serialization only
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// Attributes
public:
	CTestDoc* GetDocument();

// Operations
public:
	void DoubleBuffer(CDC* pDC);//双缓冲
	void ReadPoint();//读入顶点表
	void DrawObject(CDC* pDC,BOOL bclip);//绘制图形
	void DrawPolygon(CDC* pDC,BOOL bclip);//绘制多边形
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	void OnZoomout();
	void OnZoomin();
	int ZoomY(int y);
	int ZoomX(int x);
	BOOL onEraseBkgnd(CDC *pDC);
	BOOL ClipTest(double u,double v,double &tmax,double &tmin);
	BOOL LBLineClip();
	CP2 Convert(CPoint point);
	void DrawRect(CDC *pDC,int nScale);
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CP2 P[6];//点表	
	CP2 P1[2];//直线的两点
	CRect rect;//定义客户区
	CTransform trans;//二维变换
	// Generated message map functions
protected:
	double nRHWidth;//裁剪图形的半宽度
	double nRHHeight;//裁剪图形的半高度
	UINT nScale;//放大镜子比例
	CP2 nRCenter;//裁剪图形中心坐标
	//{{AFX_MSG(CTestView)
	afx_msg void OnDrawpic();
	afx_msg void OnLeft();
	afx_msg void OnRight();
	afx_msg void OnUp();
	afx_msg void OnDown();
	afx_msg void OnIncrease();
	afx_msg void OnDecrease();
	afx_msg void OnCounterClockwise();
	afx_msg void OnClockwise();
	afx_msg void OnXaxis();
	afx_msg void OnYaxis();
	afx_msg void OnOrg();
	afx_msg void OnYPlus();
	afx_msg void OnXNeg();
	afx_msg void OnXPlus();
	afx_msg void OnYNeg();
	afx_msg void OnReset();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnCancelMode();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in TestView.cpp
inline CTestDoc* CTestView::GetDocument()
   { return (CTestDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTVIEW_H__A75FDCFB_621C_4E38_A154_C344803E6372__INCLUDED_)
